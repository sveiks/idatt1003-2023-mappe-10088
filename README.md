# Portfolio project IDATA1003 - 2023

STUDENT NAME = Svein Kåre Sørestad  
STUDENT ID = 10088

## Project description

This project is a Java application for managing train departures from a single station. The system is operated from the command line, by navigating a simple menu.

## Project structure

The project uses the Maven build system [^1], so the application source files and test files are stored in separate trees. The main tree contains all the classes used in the application, while the test tree contains the relevant tests for the application. 

## Link to repository

https://gitlab.stud.idi.ntnu.no/sveiks/idatt1003-2023-mappe-10088

## How to run the project

The project can be run one of two ways:
1. The project can be opened in an IDE like IntelliJ, and the TrainDispatchApp source file can be run. 
2. The project can be packaged with Maven using "mvn clean package" from the terminal (Maven[^1] terminal tool is required), and then run using the command "java -cp target/TrainDispatchSystem-1.0.jar edu.ntnu.stud.TrainDispatchApp". Remember to navigate to the project root directory before running these commands. 

## How to run the tests

This project utilizes JUnit for unit-testing. The simplest way of running the tests is using the Maven[^1] terminal tool, and running the command "mvn clean test" from the project root directory. Alternatively, the tests can be run individually from an IDE like IntelliJ. 

## References

[^1]: https://maven.apache.org/
