package edu.ntnu.stud.data;

import edu.ntnu.stud.model.Schedule;
import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.railwayinfo.Destination;
import edu.ntnu.stud.railwayinfo.Line;
import edu.ntnu.stud.railwayinfo.Track;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TrainDepartureManagerTest {
  private TrainDepartureManager manager;
  private LocalTime currentTime = LocalTime.of(12, 30);
  private final TrainDeparture departure1 = new TrainDeparture("L100", new Schedule(LocalTime.of(13, 0)), Line.F1, Track.TRACK_1, Destination.OSLO);
  private final TrainDeparture departure2 = new TrainDeparture("L110", new Schedule(LocalTime.of(13, 30)), Line.F2, Track.TRACK_2, Destination.STAVANGER);
  private final TrainDeparture departure3 = new TrainDeparture("L120", new Schedule(LocalTime.of(12, 35)), Line.F3, Track.TRACK_3, Destination.DRAMMEN);
  private final TrainDeparture departure4 = new TrainDeparture("L130", new Schedule(LocalTime.of(14, 30)), Line.G1, Track.TRACK_4, Destination.TRONDHEIM);
  private final TrainDeparture departure5 = new TrainDeparture("L140", new Schedule(LocalTime.of(15, 0)), Line.G2, Track.TRACK_5, Destination.KONGSBERG);

  @BeforeEach
  void setUp() {
    departure4.setDelay(LocalTime.of(0, 35));
    manager = new TrainDepartureManager(currentTime);
    manager.add(departure1);
    manager.add(departure2);
    manager.add(departure3);
    manager.add(departure4);
    manager.add(departure5);
  }

  @Nested
  class ConstructorWorksAsIntended {
    @Test
    void constructorAcceptsValidArgument() {
      assertDoesNotThrow(() -> new TrainDepartureManager(LocalTime.of(12, 0)));
    }

    @Test
    void constructorThrowsExceptionWhenArgumentIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDepartureManager(null));
    }
  }

  @Nested
  class GetMethodsWorkAsIntended {
    @Test
    void getTrainDepartureReturnsCorrectsObject() {
      assertEquals(departure1, manager.getTrainDeparture("L100"));
      assertEquals(departure2, manager.getTrainDeparture("L110"));
      assertEquals(departure3, manager.getTrainDeparture("L120"));
      assertEquals(departure4, manager.getTrainDeparture("L130"));
      assertEquals(departure5, manager.getTrainDeparture("L140"));
    }

    @Test
    void getTrainDepartureReturnsNullIfThereIsNoMatch() {
      assertNull(manager.getTrainDeparture("NaN"));
    }

    @Test
    void getTrainDepartureThrowsExceptionWhenArgumentIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.getTrainDeparture(null));
    }

    @Test
    void searchByDestinationThrowsExceptionWhenArgumentIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.searchByDestination(null));
    }

    @Test
    void searchByDestinationReturnsDeparturesMatchingSearchTerm() {
      Iterator<TrainDeparture> departures = manager.searchByDestination("Oslo");
      assertEquals(departure1, departures.next());
    }

    @Test
    void searchByDestinationReturnsPartiallyMatchingDepartures() {
      Iterator<TrainDeparture> departures = manager.searchByDestination("on");
      List<TrainDeparture> expected = new ArrayList<>();
      expected.add(departure4);
      expected.add(departure5);

      while(departures.hasNext()) {
        assertTrue(expected.contains(departures.next()));
      }
    }

    @Test
    void searchByDestinationReturnsEmptyIteratorWhenGivenNonExistingDestination() {
      assertFalse(manager.searchByDestination("Invalid Destination").hasNext());
    }

    @Test
    void getAllDeparturesReturnsEmptyIteratorIfThereAreNoDepartures() {
      TrainDepartureManager emptyManager = new TrainDepartureManager(LocalTime.of(14, 0));
      assertFalse(emptyManager.getAllDepartures().hasNext());
    }

    @Test
    void getAllDeparturesIsSortedCorrectly() {
      List<TrainDeparture> correctOrder = new ArrayList<>();
      correctOrder.add(departure3);
      correctOrder.add(departure1);
      correctOrder.add(departure2);
      correctOrder.add(departure5);
      correctOrder.add(departure4);
      Iterator<TrainDeparture> expected = correctOrder.iterator();
      Iterator<TrainDeparture> actual = manager.getAllDepartures();

      while(expected.hasNext()) {
        assertEquals(expected.next(), actual.next());
      }
    }

    @Test
    void getCurrentTimeReturnsCurrentTimeOfSystem() {
      assertEquals(currentTime, manager.getCurrentTime());
    }

    @Test
    void getAllTrainNumbersMatchRegisteredTrainNumbers() {
      Collection<String> expected = new ArrayList<>(Arrays.asList(
          "L100",
          "L110",
          "L120",
          "L130",
          "L140"
      ));
      Collection<String> actual = manager.getAllTrainNumbers();
      assertEquals(actual.size(), expected.size());
      assertTrue(actual.containsAll(expected));
      assertTrue(expected.containsAll(actual));
    }
  }

  @Nested
  class SetMethodsWorkAsIntended {
    @Test
    void addReturnsSuccessIfDepartureIsAddedSuccessfully() {
      TrainDepartureManager.Status expected = TrainDepartureManager.Status.SUCCESS;
      assertEquals(expected, manager.add(new TrainDeparture("L200", new Schedule(LocalTime.of(13, 0)), Line.F1, Track.TRACK_1, Destination.OSLO)));
    }

    @Test
    void addReturnsExistingTrainNumberIfTrainNumberAlreadyExists() {
      TrainDepartureManager.Status expected = TrainDepartureManager.Status.EXISTING_TRAIN_NUMBER;
      assertEquals(expected, manager.add(departure1));
    }

    @Test
    void addReturnsDepartureHasLeftIfDepartureTimeIsBeforeCurrentTime() {
      TrainDepartureManager.Status expected = TrainDepartureManager.Status.DEPARTURE_HAS_LEFT;
      TrainDeparture invalidDeparture = new TrainDeparture("L400", new Schedule(LocalTime.of(11, 0)), Line.F1, Track.TRACK_1, Destination.OSLO);
      assertEquals(expected, manager.add(invalidDeparture));
    }

    @Test
    void addThrowsExceptionWhenArgumentIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.add(null));
    }

    @Test
    void updateTimeThrowsExceptionWhenArgumentIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.updateTime(null));
    }

    @Test
    void updateTimeThrowsExceptionWhenNewTimeIsBeforeTheOldOne() {
      LocalTime newTime = LocalTime.of(11, 0);
      assertThrows(IllegalArgumentException.class, () -> manager.updateTime(newTime));
    }

    @Test
    void updateTimeRemovesCorrectDepartures() {
      List<TrainDeparture> expected = new ArrayList<>();
      expected.add(departure3);
      expected.add(departure4);
      expected.add(departure5);
      manager.updateTime(LocalTime.of(13, 45));

      Iterator<TrainDeparture> actual = manager.getAllDepartures();
      while(actual.hasNext()) {
        assertTrue(expected.contains(actual.next()));
      }
    }

    @Test
    void updateTimeAccountsForDelays() {
      List<TrainDeparture> expected = new ArrayList<>();
      expected.add(departure4);
      expected.add(departure5);
      manager.updateTime(LocalTime.of(15, 0));

      Iterator<TrainDeparture> actual = manager.getAllDepartures();
      while(actual.hasNext()) {
        assertTrue(expected.contains(actual.next()));
      }
    }

    @Test
    void setDelayReturnsTrueWithValidParameters() {
      LocalTime delay = LocalTime.of(0, 5);
      assertTrue(manager.setDelay("L100", delay));
      assertEquals(delay, manager.getTrainDeparture("L100").getSchedule().getDelay());
    }

    @Test
    void setDelayThrowsExceptionWhenTrainNumberIsNull() {
      LocalTime delay = LocalTime.of(0, 0);
      assertThrows(IllegalArgumentException.class, () -> manager.setDelay(null, delay));
    }

    @Test
    void setDelaySetsDelayToNoneWhenDelayParameterIsNull() {
      LocalTime expected = LocalTime.of(0, 0);
      manager.setDelay("L130", null);
      assertEquals(expected, manager.getTrainDeparture("L130").getSchedule().getDelay());
    }

    @Test
    void setDelayReturnsFalseWhenTrainNumberDoesNotExist() {
      LocalTime delay = LocalTime.of(0, 0);
      assertFalse(manager.setDelay("Invalid Train number", delay));
    }

    @Test
    void setDelayReturnsFalseIfDelayCausesRollover() {
      LocalTime invalidDelay = LocalTime.of(23, 59);
      assertFalse(manager.setDelay("L100", invalidDelay));
    }

    @Test
    void setTrackReturnsTrueWhenTrackIsSetWithoutIssue() {
      Track expected = Track.TRACK_2;
      assertTrue(manager.setTrack("L100", expected));
      assertEquals(expected, manager.getTrainDeparture("L100").getTrack());
    }

    @Test
    void setTrackThrowsExceptionWhenTrainNumberIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.setTrack(null, Track.NO_TRACK));
    }

    @Test
    void setTrackThrowsExceptionWhenTrackIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.setTrack("L100", null));
    }

    @Test
    void setTrackReturnsFalseForNonExistantTrainNumber() {
      assertFalse(manager.setTrack("Invalid Number", Track.TRACK_1));
    }

    @Test
    void removeReturnsTrueWhenDepartureIsRemoved() {
      assertTrue(manager.remove("L100"));
      assertNull(manager.getTrainDeparture("L100"));
    }

    @Test
    void removeReturnsFalseWhenGivenNonExistingTrainNumber() {
      assertFalse(manager.remove("Not a number"));
    }

    @Test
    void removeThrowsExceptionWhenParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> manager.remove(null));
    }
  }
}
