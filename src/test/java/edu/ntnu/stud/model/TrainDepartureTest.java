package edu.ntnu.stud.model;

import edu.ntnu.stud.railwayinfo.Destination;
import edu.ntnu.stud.railwayinfo.Line;
import edu.ntnu.stud.railwayinfo.Track;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TrainDepartureTest {
  private String trainNumber;
  private Schedule schedule;
  private Line line;
  private Track track;
  private Destination destination;
  private TrainDeparture departure;

  @BeforeEach
  void setUp() {
    trainNumber = "A123";
    schedule = new Schedule(LocalTime.of(13, 0), LocalTime.of(0, 5));
    line = Line.F1;
    track = Track.TRACK_1;
    destination = Destination.STAVANGER;
    departure = new TrainDeparture(trainNumber, schedule, line, track, destination);
  }

  @Nested
  class GetMethodsReturnCorrectValues {
    @Test
    void getTrainNumberReturnsExpectedValue() {
      assertEquals(trainNumber, departure.getTrainNumber());
    }

    @Test
    void getScheduleReturnsExpectedValue() {
      assertEquals(schedule, departure.getSchedule());
    }

    @Test
    void getLineReturnsExpectedValue() {
      assertEquals(line, departure.getLine());
    }

    @Test
    void getTrackReturnsExpectedValue() {
      assertEquals(track, departure.getTrack());
    }

    @Test
    void getDestinationReturnsExpectedValue() {
      assertEquals(destination, departure.getDestination());
    }

    @Test
    void getScheduleCannotBeUsedToChangeDelay() {
      departure.getSchedule().setDelay(LocalTime.of(1, 0));
      assertEquals(schedule, departure.getSchedule());
    }
  }

  @Nested
  class SetMethodsWorkAsIntended {
    @Test
    void setDelayCorrectlyUpdatesDelay() {
      LocalTime expectedDelay = LocalTime.of(0, 10);
      departure.setDelay(expectedDelay);
      assertEquals(expectedDelay, departure.getSchedule().getDelay());
    }

    @Test
    void setDelaySetsDelayTo0WhenGivenNull() {
      LocalTime expected = LocalTime.of(0, 0);
      departure.setDelay(null);
      assertEquals(expected, departure.getSchedule().getDelay());
    }

    @Test
    void setTrackCorrectlyUpdatesTrack() {
      Track expected = Track.NO_TRACK;
      departure.setTrack(expected);
      assertEquals(expected, departure.getTrack());
    }

    @Test
    void setTrackThrowsExceptionWhenTrackIsNull() {
      assertThrows(IllegalArgumentException.class, () -> departure.setTrack(null));
    }
  }

  @Nested
  class ConstructorThrowsExceptionsWhenGivenInvalidParameters {
    @Test
    void trainNumberParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(null, schedule, line, track, destination));
    }

    @Test
    void trainNumberParameterIsBlank() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture("", schedule, line, track, destination));
    }

    @Test
    void scheduleParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(trainNumber, null, line, track, destination));
    }

    @Test
    void lineParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(trainNumber, schedule, null, track, destination));
    }

    @Test
    void trackParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(trainNumber, schedule, line, null, destination));
    }

    @Test
    void destinationParameterIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new TrainDeparture(trainNumber, schedule, line, track, null));
    }
  }
}
