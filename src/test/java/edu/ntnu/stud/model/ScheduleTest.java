package edu.ntnu.stud.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ScheduleTest {
  private Schedule schedule;
  private Schedule scheduleWithDelay;
  private LocalTime departureTime;
  private LocalTime delay;
  private LocalTime newDelay;
  private LocalTime longDelay;

  @BeforeEach
  void setUp() {
    schedule = new Schedule(LocalTime.of(12, 30));
    scheduleWithDelay = new Schedule(LocalTime.of(12, 30), LocalTime.of(0, 30));
    departureTime = LocalTime.of(12, 30);
    delay = LocalTime.of(0, 30);
    newDelay = LocalTime.of(0, 40);
    longDelay = LocalTime.of(23, 59);
  }

  @Nested
  class GetMethods {
    @Test
    void getPlannedDepartureReturnsDepartureTime() {
      assertEquals(departureTime, schedule.getPlannedDeparture());
    }

    @Test
    void getDelayReturnsDelay() {
      assertEquals(delay, scheduleWithDelay.getDelay());
    }
  }

  @Nested
  class SetMethods {
    @Test
    void setDelaySetsCorrectDelay() {
      schedule.setDelay(newDelay);
      assertEquals(newDelay, schedule.getDelay());
    }

    @Test
    void setDelayReturnsFalseIfDelayCausesRollover() {
      assertFalse(schedule.setDelay(longDelay));
    }
  }

  @Nested
  class ExceptionsAreThrownWhenGivenInvalidParameters {
    @Test
    void standardConstructorThrowsExceptionWhenDepartureTimeIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Schedule((LocalTime) null));
    }

    @Test
    void delayConstructorThrowsExceptionWhenDepartureTimeIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Schedule(null, delay));
    }

    @Test
    void delayConstructorThrowsExceptionWhenDelayIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Schedule(departureTime, null));
    }

    @Test
    void copyConstructorThrowsExceptionWhenScheduleIsNull() {
      assertThrows(IllegalArgumentException.class, () -> new Schedule((Schedule) null));
    }

    @Test
    void setDelayThrowsExceptionWhenDelayIsNull() {
      assertThrows(IllegalArgumentException.class, () -> scheduleWithDelay.setDelay(null));
    }

    @Test
    void constructorThrowsExceptionWhenDelayCausesRollover() {
      assertThrows(IllegalArgumentException.class, () -> new Schedule(departureTime, longDelay));
    }
  }

  @Nested
  class BusinessLogic {
    @Test
    void getActualDepartureAddsDelay() {
      LocalTime expected = LocalTime.of(13, 0);
      assertEquals(expected, scheduleWithDelay.getActualDeparture());
    }

    @Test
    void copyConstructorCreatesLogicallyEqualObject() {
      Schedule copy = new Schedule(schedule);
      assertEquals(schedule, copy);
    }

    @Test
    void delayConstructorSetsDelayCorrectly() {
      Schedule actual = new Schedule(departureTime, delay);
      assertEquals(delay, actual.getDelay());
    }
  }
}
