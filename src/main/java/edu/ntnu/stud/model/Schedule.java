package edu.ntnu.stud.model;

import java.time.LocalTime;
import java.util.Objects;

/**
 * Represents a schedule for a departure.
 */
public final class Schedule {
  private final LocalTime departureTime;
  private LocalTime delay;

  /**
   * Creates a new schedule with the given departure time, and no delay.
   *
   * @param departureTime (LocalTime) The planned departure time.
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public Schedule(final LocalTime departureTime) {
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time parameter cannot be null.");
    }
    this.departureTime = departureTime;
    this.delay = LocalTime.of(0, 0);
  }

  /**
   * Creates a new schedule with the given departure time and delay.
   *
   * @param departureTime (LocalTime) The planned departure time.
   * @param delay (LocalTime) The delay of the departure.
   * @throws IllegalArgumentException if any of the parameters are {@code null},
   *                                  or if the delay causes rollover to the next day.
   */
  public Schedule(final LocalTime departureTime, final LocalTime delay) {
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time cannot be null. ");
    }
    if (delay == null) {
      throw new IllegalArgumentException("Delay parameter cannot be null. ");
    }
    this.departureTime = departureTime;
    if (!setDelay(delay)) {
      throw new IllegalArgumentException("Delay cannot cause rollover to the next day. ");
    }
  }

  /**
   * Copy constructor.
   *
   * @param schedule (Schedule)
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public Schedule(final Schedule schedule) {
    if (schedule == null) {
      throw new IllegalArgumentException("Schedule parameter cannot be null.");
    }
    this.departureTime = schedule.departureTime;
    this.delay = schedule.delay;
  }

  /**
   * Gets the planned departure time.
   *
   * @return Planned time of departure.
   */
  public LocalTime getPlannedDeparture() {
    return departureTime;
  }

  public LocalTime getDelay() {
    return delay;
  }

  /**
   * Sets a delay for the departure.
   *
   * @param delay (LocalTime)
   * @return {@code true} if the delay is set without issue,
   *         {@code false} if the delay causes rollover to the next day.
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public boolean setDelay(final LocalTime delay) {
    if (delay == null) {
      throw new IllegalArgumentException("Delay parameter cannot be null. ");
    }
    int totalHours = departureTime.getHour() + delay.getHour();
    int minutes = departureTime.getMinute() + delay.getMinute();
    totalHours += minutes / 60;
    if (totalHours >= 24) {
      return false;
    }
    this.delay = delay;
    return true;
  }

  /**
   * Gets the actual departure time, which includes the delay.
   *
   * @return The actual departure time.
   */
  public LocalTime getActualDeparture() {
    int hours = departureTime.getHour() + delay.getHour();
    int minutes = departureTime.getMinute() + delay.getMinute();
    hours += minutes / 60;
    minutes = minutes % 60;
    return LocalTime.of(hours, minutes);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Schedule schedule = (Schedule) o;
    return Objects.equals(departureTime, schedule.departureTime)
        && Objects.equals(delay, schedule.delay);
  }

  @Override
  public int hashCode() {
    return Objects.hash(departureTime, delay);
  }
}
