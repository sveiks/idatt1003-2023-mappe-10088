package edu.ntnu.stud.model;

import edu.ntnu.stud.railwayinfo.Destination;
import edu.ntnu.stud.railwayinfo.Line;
import edu.ntnu.stud.railwayinfo.Track;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Represents a train departure in a railway system.
 */
public final class TrainDeparture {
  private final String trainNumber;
  private final Schedule schedule;
  private final Line line;
  private Track track;
  private final Destination destination;

  /**
   * Creates a new train departure with information relevant for passengers.
   *
   * @param trainNumber (String)      A number identifying the departure.
   * @param schedule (Schedule)       The schedule for the departure, including potential delays.
   * @param line (Line)               The line the train is set to travel on.
   * @param track (Track)             Which track the train is departing from.
   * @param destination (Destination) Where the train is headed.
   * @throws IllegalArgumentException if any of the parameters are {@code null},
   *                                  or if the trainNumber parameter is blank.
   */
  public TrainDeparture(final String trainNumber,
                        final Schedule schedule,
                        final Line line,
                        final Track track,
                        final Destination destination) {
    if (trainNumber == null) {
      throw new IllegalArgumentException("Train number parameter cannot be null. ");
    } else if (trainNumber.isBlank()) {
      throw new IllegalArgumentException("Train number parameter cannot be blank. ");
    }
    if (schedule == null) {
      throw new IllegalArgumentException("Schedule parameter cannot be null. ");
    }
    if (line == null) {
      throw new IllegalArgumentException("Line parameter cannot be null. ");
    }
    if (destination == null) {
      throw new IllegalArgumentException("Destination parameter cannot be null. ");
    }
    this.trainNumber = trainNumber;
    this.schedule = new Schedule(schedule);
    this.line = line;
    setTrack(track);
    this.destination = destination;
  }

  /**
   * Copy constructor. Performs a deep copy of the parameter object.
   *
   * @param departure (TrainDeparture) Departure to be copied.
   */
  public TrainDeparture(final TrainDeparture departure) {
    if (departure == null) {
      throw new IllegalArgumentException("Departure parameter cannot be null. ");
    }
    this.trainNumber = departure.trainNumber;
    this.schedule = new Schedule(departure.schedule);
    this.line = departure.line;
    this.track = departure.track;
    this.destination = departure.destination;
  }

  public String getTrainNumber() {
    return trainNumber;
  }

  public Schedule getSchedule() {
    return new Schedule(schedule);
  }

  public Line getLine() {
    return line;
  }

  public Track getTrack() {
    return track;
  }

  public Destination getDestination() {
    return destination;
  }

  /**
   * Sets a new track for the departure.
   *
   * @param track (Track) The new track for the departure.
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public void setTrack(final Track track) {
    if (track == null) {
      throw new IllegalArgumentException("Track parameter cannot be null.");
    }
    this.track = track;
  }

  /**
   * Sets the delay for the schedule of the departure.
   *
   * @param delay (LocalTime) The delay for the departure.
   *              A {@code null} value is treated as no delay.
   * @return {@code true} if the delay is set without issue,
   *         {@code false} if the delay causes rollover to the next day.
   */
  public boolean setDelay(final LocalTime delay) {
    if (delay == null) {
      return schedule.setDelay(LocalTime.of(0, 0));
    } else {
      return schedule.setDelay(delay);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrainDeparture that = (TrainDeparture) o;
    return Objects.equals(schedule, that.schedule)
        && Objects.equals(trainNumber, that.trainNumber)
        && line == that.line
        && track == that.track
        && destination == that.destination;
  }

  @Override
  public int hashCode() {
    return Objects.hash(schedule, trainNumber, line, track, destination);
  }
}
