package edu.ntnu.stud.io;

/**
 * Constants for different text colors in the terminal.
 */
public enum AnsiColor {
  STANDARD("\u001B[0m"),
  RED("\u001B[31m");
  private final String value;

  AnsiColor(final String value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return this.value;
  }
}
