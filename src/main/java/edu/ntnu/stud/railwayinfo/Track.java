package edu.ntnu.stud.railwayinfo;

/**
 * Constants representing the tracks of the station.
 */
public enum Track {
  NO_TRACK(-1),
  TRACK_1(1),
  TRACK_2(2),
  TRACK_3(3),
  TRACK_4(4),
  TRACK_5(5);

  private final int trackNumber;

  Track(int trackNumber) {
    this.trackNumber = trackNumber;
  }

  public int getTrackNumber() {
    return trackNumber;
  }
}
