package edu.ntnu.stud.railwayinfo;

/**
 * The different lines in a railway system.
 */
public enum Line {
  L1("L1"), L2("L2"), L3("L3"), L4("L4"),
  F1("F1"), F2("F2"), F3("F3"), F4("F4"),
  G1("G1"), G2("G2"), G3("G3"), G4("G4");

  private final String lineName;

  Line(String lineName) {
    this.lineName = lineName;
  }

  @Override
  public String toString() {
    return lineName;
  }
}
