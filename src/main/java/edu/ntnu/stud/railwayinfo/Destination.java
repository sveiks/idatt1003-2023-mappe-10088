package edu.ntnu.stud.railwayinfo;

/**
 * Constants representing possible destinations.
 */
public enum Destination {
  OSLO("Oslo"),
  STAVANGER("Stavanger"),
  TRONDHEIM("Trondheim"),
  DRAMMEN("Drammen"),
  KONGSBERG("Kongsberg"),
  EIDSVOLL("Eidsvoll");

  private final String name;

  Destination(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}
