package edu.ntnu.stud;

import edu.ntnu.stud.data.TrainDepartureManager;
import edu.ntnu.stud.io.AnsiColor;
import edu.ntnu.stud.model.Schedule;
import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.railwayinfo.Destination;
import edu.ntnu.stud.railwayinfo.Line;
import edu.ntnu.stud.railwayinfo.Track;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;
import java.util.Scanner;
import java.util.StringJoiner;

/**
 * The user interface for the Train Dispatch application.
 */
public class UserInterface {
  private final Scanner scanner;
  private final TrainDepartureManager manager;
  private static final String INPUT_PROMPT = "Input: ";
  private static final String ERROR_MESSAGE = "An unexpected error occurred. Please try again.";
  private static final String CANCEL = "cancel";

  /**
   * Initializes the user interface with the current time of the system set to 00:00.
   */
  public UserInterface() {
    manager = new TrainDepartureManager(LocalTime.of(0, 0));
    scanner = new Scanner(System.in);
  }

  /**
   * Initializes the application with test data.
   */
  public void init() {
    manager.add(
        new TrainDeparture(
            "A10",
            new Schedule(LocalTime.of(3, 45)),
            Line.F1,
            Track.NO_TRACK,
            Destination.STAVANGER)
    );

    manager.add(
        new TrainDeparture(
            "B15",
            new Schedule(LocalTime.of(5, 0)),
            Line.F2,
            Track.TRACK_1,
            Destination.OSLO
        )
    );

    manager.add(
        new TrainDeparture(
            "A20",
            new Schedule(LocalTime.of(7, 0)),
            Line.G3,
            Track.TRACK_4,
            Destination.STAVANGER
        )
    );
    manager.setDelay("A20", LocalTime.of(2, 30));



    manager.add(
        new TrainDeparture(
            "B55",
            new Schedule(LocalTime.of(8, 30)),
            Line.G4,
            Track.TRACK_3,
            Destination.DRAMMEN
        )
    );
  }

  /**
   * Starts the application.
   */
  public void start() {
    System.out.println("Welcome to Railways!");
    mainMenu();
  }

  /**
   * Presents the main menu for the user,
   * and sends them to the appropriate menu based on their input.
   */
  private void mainMenu() {
    boolean running = true;
    while (running) {
      System.out.printf(
          "Please select a menu option by entering the corresponding number."
              + " Current time: %02d:%02d. \n",
          manager.getCurrentTime().getHour(),
          manager.getCurrentTime().getMinute());
      System.out.println("1. View upcoming departures");
      System.out.println("2. Register a new departure");
      System.out.println("3. Search for departures");
      System.out.println("4. Edit an existing departure");
      System.out.println("5. Update clock");
      System.out.println("6. Exit");
      System.out.print(INPUT_PROMPT);

      int inputValue = getIntegerInput(1, 6);

      switch (inputValue) {
        case 1 -> viewDepartures();
        case 2 -> registerDeparture();
        case 3 -> {
          System.out.println("Search for a departure by:");
          System.out.println("1. Train number");
          System.out.println("2. Destination");
          int searchBy = getIntegerInput(1, 2);
          switch (searchBy) {
            case 1 -> searchByTrainNumber();
            case 2 -> searchByDestination();
            default -> System.out.println(ERROR_MESSAGE);
          }
        }
        case 4 -> editDeparture(null);
        case 5 -> updateTime();
        case 6 -> {
          System.out.println("Thank you for using Railways!");
          running = false;
        }
        default -> System.out.println(ERROR_MESSAGE);
      }
    }
  }

  /**
   * Prints all the currently registered departures to the terminal in a table format.
   */
  private void viewDepartures() {
    System.out.println();
    Iterator<TrainDeparture> departures = manager.getAllDepartures();
    printDepartureHeader();
    while (departures.hasNext()) {
      printDeparture(departures.next());
    }
    System.out.println("Press enter to return to main menu.");
    scanner.nextLine();
  }

  /**
   * Takes the necessary input for registering a new departure from the user,
   * and adds it to the manager according to user input.
   */
  private void registerDeparture() {
    System.out.println("Enter a unique 3 character id for the departure. "
        + "Type \"taken\" to see existing id's.");

    String trainNumber = getTrainNumberInput();
    System.out.println("Train id: " + trainNumber);
    System.out.println();

    System.out.printf("Enter the scheduled departure time. Must be at %02d:%02d or later.%n",
        manager.getCurrentTime().getHour(),
        manager.getCurrentTime().getMinute());
    LocalTime scheduledTime = getTimeInput(manager.getCurrentTime());
    System.out.printf("Scheduled departure time: %02d:%02d%n",
        scheduledTime.getHour(),
        scheduledTime.getMinute());
    System.out.println();
    Schedule schedule = new Schedule(scheduledTime);

    System.out.println("Set the line for the departure. Available choices are:");
    Line line = getLineInput();
    System.out.println("Line: " + line);
    System.out.println();

    System.out.println("Select a track (optional). Valid selections are:");
    Track track = getTrackInput();
    System.out.println("Track: " + (track == Track.NO_TRACK ? "None" : track.getTrackNumber()));
    System.out.println();

    System.out.println("Enter the destination for the departure. "
        + "Input is not case sensitive. "
        + "Type \"where\" to see a list of all possible destinations.");

    Destination destination = getDestinationInput();
    System.out.println("Destination: " + destination);
    System.out.println();


    boolean departureIsCompleted = false;
    while (!departureIsCompleted) {
      TrainDeparture departure;
      try {
        departure = new TrainDeparture(
            trainNumber,
            schedule,
            line,
            track,
            destination
        );
      } catch (Exception e) {
        System.out.println(ERROR_MESSAGE);
        return;
      }
      printDepartureHeader();
      printDeparture(departure);
      System.out.println("Type \"ok\" to confirm that the information above is correct, "
          + "or \"edit\" to make changes. "
          + "Type \"cancel\" to discard the changes and return to the main menu. ");
      String input = scanner.nextLine();
      switch (input.toLowerCase()) {
        case "ok" -> {
          try {
            TrainDepartureManager.Status status = manager.add(departure);
            switch (status) {
              case SUCCESS -> {
                manager.add(departure);
                System.out.println("Departure successfully registered.");
                departureIsCompleted = true;
              }
              case EXISTING_TRAIN_NUMBER -> {
                System.out.println("That train number is already registered. "
                    + "Please enter a different one.");
                trainNumber = getTrainNumberInput();
              }
              case DEPARTURE_HAS_LEFT -> {
                System.out.println("The scheduled time for the departure is before the "
                    + "current time of day. Please enter a new time.");
                schedule = new Schedule(getTimeInput(manager.getCurrentTime()));
              }
              default -> throw new IllegalStateException("Invalid status code from manager.");
            }
          } catch (Exception e) {
            System.out.println(ERROR_MESSAGE + ": " + e.getMessage());
          }
        }

        case "edit" -> {
          System.out.println("Enter the value you would like to change "
              + "(use the same name as in the header above)");
          String editValue = scanner.nextLine();
          switch (editValue.toLowerCase()) {
            case "time" -> schedule = new Schedule(getTimeInput(manager.getCurrentTime()));
            case "train nr" -> trainNumber = getTrainNumberInput();
            case "line" -> line = getLineInput();
            case "destination" -> destination = getDestinationInput();
            case "track" -> track = getTrackInput();
            default -> System.out.println(AnsiColor.RED
                + "Invalid selection."
                + AnsiColor.STANDARD);
          }
        }

        case CANCEL -> {
          System.out.println("Departure creation aborted.");
          departureIsCompleted = true;
        }

        default -> System.out.println(AnsiColor.RED
            + "Invalid input, please try again." + AnsiColor.STANDARD);
      }
    }
  }

  /**
   * Helper method for finding a departure based on its train number.
   *
   * @return the departure, or {@code null} if the user cancels.
   */
  private TrainDeparture findDeparture() {
    System.out.println("Enter the train number of the departure you would like to find. "
        + "Type \"cancel\" to return to the main menu.");
    boolean inputIsValid = false;
    TrainDeparture departure = null;
    while (!inputIsValid) {
      System.out.print(INPUT_PROMPT);
      String input = scanner.nextLine();
      if (input.equalsIgnoreCase(CANCEL)) {
        System.out.println();
        return null;
      }
      if (manager.getAllTrainNumbers().contains(input)) {
        departure = manager.getTrainDeparture(input);
        if (departure == null) {
          throw new NullPointerException(
              "Mismatch between getAllTrainNumbers and registered train number keys.");
        }
        inputIsValid = true;
      } else {
        System.out.printf("Could not find a departure matching the train number \"%s\". "
            + "Please try again.%n", input);
      }
    }
    return departure;
  }

  /**
   * Allows the user to search for a departure based on train number,
   * and give them the option to edit it.
   */
  private void searchByTrainNumber() {
    TrainDeparture departure = findDeparture();
    if (departure == null) {
      return;
    }
    printDepartureHeader();
    printDeparture(departure);
    System.out.println("Press enter to return to the main menu, "
        + "or type \"edit\" to make changes to this departure.");
    String choice = scanner.nextLine();
    if (choice.equalsIgnoreCase("edit")) {
      editDeparture(departure);
    }
  }

  /**
   * Presents the departures matching the destination search query of the user in a table.
   */
  private void searchByDestination() {
    System.out.println("Enter your search term, or type \"where\" to see all destinations. "
        + "Partial matches are included. Type \"cancel\" to return to main menu.");
    boolean inputIsValid = false;
    while (!inputIsValid) {
      System.out.print(INPUT_PROMPT);
      String input = scanner.nextLine();
      System.out.println();
      if (input.equalsIgnoreCase(CANCEL)) {
        return;
      }
      if (input.equalsIgnoreCase("where")) {
        Arrays.stream(Destination.values()).sorted().forEach(System.out::println);
      } else if (input.isBlank()) {
        System.out.println("Please enter an input.");
      } else {
        inputIsValid = true;
      }
      if (!inputIsValid) {
        continue;
      }
      Iterator<TrainDeparture> departures = manager.searchByDestination(input);
      if (!departures.hasNext()) {
        System.out.println("No matches were found for that search term. ");
        inputIsValid = false;
      } else {
        printDepartureHeader();
        while (departures.hasNext()) {
          printDeparture(departures.next());
        }
      }
    }
    System.out.println("Press enter to return to main menu.");
    scanner.nextLine();
  }

  /**
   * Allows the user to make certain changes to a departure.
   *
   * @param departure (TrainDeparture) The departure to edit,
   *                  or {@code null} if the user needs to select one.
   */
  private void editDeparture(TrainDeparture departure) {
    if (departure == null) {
      departure = findDeparture();
    }
    if (departure == null) { //A null value here means the user wants to return to the main menu.
      return;
    }
    System.out.println("In this menu you can set a delay or the track for a departure. "
        + "No other values can be changed.");
    boolean userIsFinished = false;
    while (!userIsFinished) {
      //Fetches the updated departure from the register.
      departure = manager.getTrainDeparture(departure.getTrainNumber());

      printDepartureHeader();
      printDeparture(departure);
      System.out.println("Enter the value you want to change (as it's written in the headers), "
          + "or type \"done\" to return to the main menu. "
          + "Type \"delete\" to delete the departure.");
      System.out.print(INPUT_PROMPT);
      String input = scanner.nextLine();
      switch (input.toLowerCase()) {
        case "done" -> userIsFinished = true;

        case "delay" -> {
          LocalTime delay = getTimeInput(null);
          if (!manager.setDelay(departure.getTrainNumber(), delay)) {
            System.out.println(AnsiColor.RED
                + "The delay input will cause a rollover to the next day. "
                + "Please input a delay so that the departure will leave at latest 23:59"
                + AnsiColor.STANDARD);
          }
        }

        case "track" -> {
          Track track = getTrackInput();
          manager.setTrack(departure.getTrainNumber(), track);
        }

        case "delete" -> {
          System.out.println("Are you sure you want to remove this departure? Y/N");
          input = scanner.nextLine();
          switch (input.toLowerCase()) {
            case "y" -> {
              userIsFinished = manager.remove(departure.getTrainNumber());
              String feedback = userIsFinished
                  ? "Departure successfully removed." : ERROR_MESSAGE;
              System.out.println(feedback);
            }

            case "n" -> System.out.println(AnsiColor.RED
                + "Deletion aborted, no changes were made"
                + AnsiColor.STANDARD);

            default -> System.out.println(AnsiColor.RED
                + "Invalid selection, deletion aborted."
                + AnsiColor.STANDARD);
          }
        }

        default -> System.out.println(AnsiColor.RED
            + "Invalid selection, please try again."
            + AnsiColor.STANDARD);
      }
    }
  }

  /**
   * Updates the current time of the system based on user input.
   */
  private void updateTime() {
    LocalTime currentTime = manager.getCurrentTime();

    System.out.printf(
        "%nCurrent time is %02d:%02d. Input new time (must be later than current time):%n",
        currentTime.getHour(),
        currentTime.getMinute());

    LocalTime newTime = getTimeInput(currentTime);

    try {
      manager.updateTime(newTime);
      System.out.printf("New time successfully updated to %02d:%02d%n%n",
          newTime.getHour(),
          newTime.getMinute());
    } catch (Exception e) {
      System.out.println(ERROR_MESSAGE);
    }
  }

  /**
   * Checks whether a given String represents an integer.
   *
   * @param input (String)
   * @return {@code true} if the String represents an integer, {@code false} if not.
   */
  private boolean isInteger(String input) {
    if (input == null) {
      return false;
    }
    try {
      Integer.parseInt(input);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Takes an integer input from the user in a given range.
   *
   * @param minValue (int) The minimum allowed value the user can input.
   * @param maxValue (int) The maximum allowed value the user can input.
   * @return the integer input by the user.
   */
  private int getIntegerInput(int minValue, int maxValue) {
    boolean inputIsValid = false;
    String input = "";

    while (!inputIsValid) {
      input = scanner.nextLine();
      inputIsValid = true;
      String errorMessage = "";
      if (!isInteger(input)) {
        errorMessage = "Input must be an integer.";
        inputIsValid = false;
      } else { //String can be safely parsed to integer.
        int value = Integer.parseInt(input);
        if (value < minValue) {
          inputIsValid = false;
          errorMessage = String.format("Input cannot be less than %d.", minValue);
        }
        if (value > maxValue) {
          inputIsValid = false;
          errorMessage = String.format("Input cannot be higher than %d.", maxValue);
        }
      }
      if (!errorMessage.isBlank()) {
        System.out.println(AnsiColor.RED + errorMessage + AnsiColor.STANDARD);
        System.out.print(INPUT_PROMPT);
      }
    }

    int inputValue;
    try {
      inputValue = Integer.parseInt(input);
    } catch (Exception e) {
      System.out.println(ERROR_MESSAGE);
      throw e;
    }
    return inputValue;
  }

  /**
   * Instantiates a LocalTime object based on user input.
   *
   * @param currentTime (LocalTime) the "minimum" time the user can input.
   *                    If {@code null}, all inputs are valid.
   * @return the LocalTime object corresponding to the input.
   */
  private LocalTime getTimeInput(LocalTime currentTime) {
    boolean hourIsValid = false;
    int hour = -1;
    while (!hourIsValid) {
      System.out.println("Enter hours:");
      System.out.print(INPUT_PROMPT);
      hour = getIntegerInput(0, 23);
      if (currentTime != null && hour < currentTime.getHour()) {
        System.out.println(AnsiColor.RED
            + "Hour must be " + currentTime.getHour() + " or later."
            + AnsiColor.STANDARD);
      } else {
        hourIsValid = true;
      }
    }
    System.out.println("Hour: " + hour);

    LocalTime inputTime = null;
    boolean timeIsValid = false;
    int minute;
    while (!timeIsValid) {
      System.out.println("Enter minutes:");
      System.out.print(INPUT_PROMPT);
      minute = getIntegerInput(0, 59);
      inputTime = LocalTime.of(hour, minute);
      if (currentTime != null && inputTime.isBefore(currentTime) || inputTime.equals(currentTime)) {
        System.out.printf("%sTime must be after %02d:%02d.%s%n",
            AnsiColor.RED,
            currentTime.getHour(),
            currentTime.getMinute(),
            AnsiColor.STANDARD);
      } else {
        timeIsValid = true;
      }
    }
    return inputTime;
  }

  /**
   * Takes a valid train number from the user.
   *
   * @return the train number input by the user.
   */
  private String getTrainNumberInput() {
    boolean inputIsValid = false;
    String trainNumber = "";
    while (!inputIsValid) {
      System.out.print(INPUT_PROMPT);
      trainNumber = scanner.nextLine();
      if (trainNumber.equalsIgnoreCase("taken")) {
        System.out.println("The following id's are currently in use:");
        manager.getAllTrainNumbers().forEach(System.out::println);
        continue;
      }
      if (trainNumber.isBlank()) {
        System.out.println(AnsiColor.RED
            + "Train number cannot be blank."
            + AnsiColor.STANDARD);

      } else if (trainNumber.length() != 3) {
        System.out.println(AnsiColor.RED
            + "Train number must be 3 characters."
            + AnsiColor.STANDARD);

      } else if (manager.getAllTrainNumbers().contains(trainNumber)) {
        System.out.println(AnsiColor.RED
            + "That train number is already registered."
            + AnsiColor.STANDARD);
      } else {
        inputIsValid = true;
      }
    }
    return trainNumber;
  }

  /**
   * Allows the user to select a line for a departure from a list of valid ones.
   *
   * @return the selected Line.
   */
  private Line getLineInput() {
    boolean inputIsValid = false;
    String input;
    Line line = null;
    while (!inputIsValid) {
      System.out.println("Lines:");
      StringJoiner lines = new StringJoiner(", ");
      Arrays.stream(Line.values()).forEach(l -> lines.add(l.toString()));
      System.out.println(lines);
      System.out.printf(INPUT_PROMPT);
      input = scanner.nextLine();
      if (input.isBlank()) {
        System.out.println(AnsiColor.RED
            + "The departure must be set to a line."
            + AnsiColor.STANDARD);
      } else  {
        try {
          line = Line.valueOf(input);
          inputIsValid = true;
        } catch (Exception e) {
          System.out.println(AnsiColor.RED + "Invalid selection." + AnsiColor.STANDARD);
        }
      }
    }
    return line;
  }

  /**
   * Takes an optional Track input from the user.
   *
   * @return the Track matching the selection, or {@code null} if none are selected.
   */
  private Track getTrackInput() {
    boolean inputIsValid = false;
    String input;
    Track track = null;
    while (!inputIsValid) {
      System.out.println("Tracks:");
      StringJoiner tracks = new StringJoiner(", ");
      Arrays.stream(Track.values())
          .filter(t -> t.getTrackNumber() != -1)
          .forEach(t -> tracks.add(String.valueOf(t.getTrackNumber())));
      System.out.println(tracks);
      System.out.println("Enter blank for no track.");
      System.out.print(INPUT_PROMPT);
      input = scanner.nextLine();
      if (input.isBlank()) {
        track = Track.NO_TRACK;
        inputIsValid = true;
      } else {
        try {
          int trackNumber = Integer.parseInt(input);
          Optional<Track> match = Arrays.stream(Track.values())
              .filter(t -> t.getTrackNumber() == trackNumber)
              .findFirst();
          if (match.isPresent()) {
            track = match.get();
            inputIsValid = true;
          } else {
            System.out.println(AnsiColor.RED
                + "There are no tracks matching that number."
                + AnsiColor.STANDARD);
          }
        } catch (Exception e) {
          System.out.println(AnsiColor.RED
              + "That selection is invalid. Enter an existing track, or enter blank for no track."
              + AnsiColor.STANDARD);
        }
      }
    }
    return track;
  }

  /**
   * Allows the used to select a destination based on the ones currently registered in the system.
   *
   * @return the selected Destination.
   */
  private Destination getDestinationInput() {
    boolean inputIsValid = false;
    String input;
    Destination destination = null;
    while (!inputIsValid) {
      System.out.print(INPUT_PROMPT);
      input = scanner.nextLine();
      if (input.equalsIgnoreCase("where")) {
        Arrays.stream(Destination.values()).sorted().forEach(System.out::println);
        continue;
      }
      if (input.isBlank()) {
        System.out.println(AnsiColor.RED
            + "Departure must have a destination."
            + AnsiColor.STANDARD);
      } else {
        try {
          destination = Destination.valueOf(input.toUpperCase());
          inputIsValid = true;
        } catch (Exception e) {
          System.out.println(AnsiColor.RED
              + "There are no valid destinations matching that input."
              + AnsiColor.STANDARD);
        }
      }
    }
    return destination;
  }

  /**
   * Prints a header for the departure table.
   */
  private void printDepartureHeader() {
    System.out.printf("|%1$-5s|", "Time");
    System.out.printf("%1$-5s|", "Delay");
    System.out.printf("%1$-8s|", "Train nr");
    System.out.printf("%1$-4s|", "Line");
    System.out.printf("%1$-30s|", "Destination");
    System.out.printf("%1$-5s|%n", "Track");
    System.out.println("-".repeat(64));
  }

  /**
   * Prints a departure as an entry in a table.
   *
   * @param departure (TrainDeparture)
   */
  private void printDeparture(TrainDeparture departure) {
    Schedule schedule = departure.getSchedule();
    System.out.printf("|%02d:%02d|",
        schedule.getActualDeparture().getHour(),
        schedule.getActualDeparture().getMinute());
    System.out.printf("%s|", schedule.getDelay() == LocalTime.of(0, 0)
        ? " ".repeat(5)
        : String.format("%02d:%02d",
        schedule.getDelay().getHour(),
        schedule.getDelay().getMinute()));
    System.out.printf("%1$-8s|", departure.getTrainNumber());
    System.out.printf("%1$-4s|", departure.getLine());
    System.out.printf("%1$-30s|", departure.getDestination());
    System.out.printf("%1$5s|%n",
        departure.getTrack().getTrackNumber() < 0 ? "" : departure.getTrack().getTrackNumber());
    System.out.println("-".repeat(64));
  }
}
