package edu.ntnu.stud.data;

import edu.ntnu.stud.model.TrainDeparture;
import edu.ntnu.stud.railwayinfo.Destination;
import edu.ntnu.stud.railwayinfo.Track;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * Manages train departures in a dispatch system.
 */
public final class TrainDepartureManager {

  /**
   * Status codes to facilitate feedback from methods.
   */
  public enum Status {
    SUCCESS,
    EXISTING_TRAIN_NUMBER,
    DEPARTURE_HAS_LEFT
  }

  private final Map<String, TrainDeparture> register;
  private LocalTime currentTime;
  private static final String INVALID_TRAIN_NUMBER = "Train number parameter cannot be null. ";

  /**
   * Instantiates a new manager for train departures,
   * and sets the current time to the parameter value.
   *
   * @param currentTime (LocalTime) The current time of the day.
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public TrainDepartureManager(final LocalTime currentTime) {
    if (currentTime == null) {
      throw new IllegalArgumentException("Current time parameter cannot be null. ");
    }
    register = new HashMap<>();
    this.currentTime = currentTime;
  }

  /**
   * Adds a new train departure to the register.
   * The number of the departure cannot be the same as an existing one.
   *
   * @param departure (TrainDeparture) The departure to be added.
   * @return The method returns a constant indicating the status of the operation:
   *          <ul>
   *            <li>EXISTING_TRAIN_NUMBER if the train number is already registered.</li>
   *            <li>DEPARTURE_HAS_LEFT if the departure time is before the current time.</li>
   *            <li>SUCCESS if the departure is added without issue.</li>
   *          </ul>
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public Status add(final TrainDeparture departure) {
    if (departure == null) {
      throw new IllegalArgumentException("Departure parameter cannot be null.");
    }
    String trainNumber = departure.getTrainNumber();
    if (register.containsKey(trainNumber)) {
      return Status.EXISTING_TRAIN_NUMBER;
    }
    if (departure.getSchedule().getActualDeparture().isBefore(currentTime)) {
      return Status.DEPARTURE_HAS_LEFT;
    }
    register.put(departure.getTrainNumber(), new TrainDeparture(departure));
    return Status.SUCCESS;
  }


  /**
   * Removes the departure identified by the given train number from the register.
   *
   * @param trainNumber (String) A number uniquely identifying a departure.
   * @return {@code true} if the departure is successfully removed,
   *         {@code false} if the departure does not exist.
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public boolean remove(final String trainNumber) {
    if (trainNumber == null) {
      throw new IllegalArgumentException(INVALID_TRAIN_NUMBER);
    }
    TrainDeparture removed = register.remove(trainNumber);
    return removed != null;
  }

  /**
   * Searches for a departure based on its number, and returns the results.
   *
   * @param trainNumber (String) A number uniquely identifying the departure.
   * @return The departure if it exists, {@code null} if it doesn't.
   * @throws IllegalArgumentException if the parameter is {@code null}.
   */
  public TrainDeparture getTrainDeparture(final String trainNumber) {
    if (trainNumber == null) {
      throw new IllegalArgumentException(INVALID_TRAIN_NUMBER);
    }
    TrainDeparture departure = register.get(trainNumber);
    return departure == null ? null : new TrainDeparture(departure);
  }

  /**
   * <p>Searches for all departures going to the destination matching the search term.</p>
   * Partial matches are included, and search is <b>not</b> case-sensitive.
   *
   * @param searchTerm (String) The destination to search for.
   * @return An iterator containing the departures found.
   * @throws IllegalArgumentException if the searchTerm parameter is {@code null}.
   */
  public Iterator<TrainDeparture> searchByDestination(final String searchTerm) {
    if (searchTerm == null) {
      throw new IllegalArgumentException("Search term parameter cannot be null. ");
    }
    List<Destination> destinations = Arrays
        .stream(Destination.values())
        .filter(d -> d.toString().toLowerCase().contains(searchTerm.toLowerCase()))
        .toList();

    List<TrainDeparture> departures = new ArrayList<>();

    if (destinations.isEmpty()) {
      return departures.iterator();
    }

    register.values()
        .stream()
        .filter(d -> destinations.contains(d.getDestination()))
        .forEach(d -> departures.add(new TrainDeparture(d)));

    return departures.iterator();
  }

  /**
   * Gets the current time that the system is set to.
   *
   * @return current time
   */
  public LocalTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Updates the current time, and removes any departures that have departed since last time.
   *
   * @param newTime (LocalTime) The current time of the day.
   * @throws IllegalArgumentException if the parameter is {@code null},
   *                                  or if the new time if before the current time.
   */
  public void updateTime(final LocalTime newTime) {
    if (newTime == null) {
      throw new IllegalArgumentException("New time parameter cannot be null. ");
    } else if (newTime.isBefore(currentTime)) {
      throw new IllegalArgumentException("New time must be after the current time. ");
    }
    currentTime = newTime; //LocalTime objects are immutable, so this if fine.
    List<TrainDeparture> toBeRemoved = register.values()
        .stream()
        .filter(d -> d.getSchedule().getActualDeparture().isBefore(newTime)).toList();
    //Stream cannot be used here due to concurrency issues.
    for (TrainDeparture departure : toBeRemoved) {
      register.remove(departure.getTrainNumber());
    }
  }

  /**
   * Sets the track for a given train number.
   *
   * @param trainNumber (String) The departure to edit.
   * @param track (Track) The new track for the departure.
   * @return {@code true} if the track is successfully set,
   *         {@code false} if the train number does not exist.
   */
  public boolean setTrack(final String trainNumber, final Track track) {
    if (trainNumber == null) {
      throw new IllegalArgumentException(INVALID_TRAIN_NUMBER);
    }
    if (track == null) {
      throw new IllegalArgumentException("Track parameter cannot be null. ");
    }
    TrainDeparture departure = register.get(trainNumber);
    if (departure == null) {
      return false;
    }
    departure.setTrack(track);
    return true;
  }

  /**
   * Sets a delay for a given departure.
   * Note that the delay cannot be so long as to cause a rollover to the next day.
   *
   * @param trainNumber (String) The departure to edit.
   * @param delay (LocalTime) The new delay for the departure.
   *              A {@code null} value is treated as no delay.
   * @return {@code true} if the delay is set without issue,
   *         {@code false} if the departure does not exist, or if the delay causes rollover.
   */
  public boolean setDelay(String trainNumber, LocalTime delay) {
    if (trainNumber == null) {
      throw new IllegalArgumentException(INVALID_TRAIN_NUMBER);
    }
    TrainDeparture departure = register.get(trainNumber);
    if (departure == null) {
      return false;
    }
    return departure.setDelay(delay);
  }

  /**
   * Returns an iterator of all the train departures, sorted in ascending order by departure time.
   *
   * @return A sorted iterator of all the departures.
   */
  public Iterator<TrainDeparture> getAllDepartures() {
    //Although it is not strictly necessary to have a case for equal departure times,
    //it is done to maintain the contract of the compare method.
    return register.values()
        .stream()
        .sorted((a, b) -> {
          if (a.getSchedule().getActualDeparture().equals(b.getSchedule().getActualDeparture())) {
            return 0;
          } else {
            return a.getSchedule().getActualDeparture()
                .isBefore(b.getSchedule().getActualDeparture()) ? -1 : 1;
          }
        })
        .iterator();
  }

  /**
   * Gets all the train numbers of the departures currently registered.
   *
   * @return a Collection of unique train numbers.
   */
  public Collection<String> getAllTrainNumbers() {
    return new ArrayList<>(register.keySet());
  }
}
